﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyVod.Infrastructure.Marketing;

#nullable disable

namespace MyVod.Migrations.SubscriptionTypeDb
{
    [DbContext(typeof(SubscriptionTypeDbContext))]
    [Migration("20220612180040_AddSeedData")]
    partial class AddSeedData
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.5");

            modelBuilder.Entity("MyVod.Domain.Marketing.Domain.SubscriptionType", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<uint>("Price")
                        .HasColumnType("INTEGER");

                    b.Property<TimeSpan>("TimeSpan")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SubscriptionTypes");

                    b.HasData(
                        new
                        {
                            Id = new Guid("4bcb5575-a3b0-48cd-9199-beec567433cc"),
                            Price = 10000u,
                            TimeSpan = new TimeSpan(30, 0, 0, 0, 0)
                        },
                        new
                        {
                            Id = new Guid("9abd91da-9cac-4b4b-9a8f-2ecb3fd01090"),
                            Price = 55000u,
                            TimeSpan = new TimeSpan(180, 0, 0, 0, 0)
                        },
                        new
                        {
                            Id = new Guid("37dc0eaf-3a34-49a2-8208-e67539fd078e"),
                            Price = 80000u,
                            TimeSpan = new TimeSpan(360, 0, 0, 0, 0)
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
