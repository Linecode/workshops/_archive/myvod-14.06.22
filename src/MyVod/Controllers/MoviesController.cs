using Microsoft.AspNetCore.Mvc;
using MyVod.Facades;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Controllers;

public class MoviesController : Controller
{
    [Obsolete]
    private readonly IMovieService _movieService;
    private readonly IMovieFacade _movieFacade;

    public MoviesController(IMovieService movieService, IMovieFacade movieFacade)
    {
        _movieService = movieService;
        _movieFacade = movieFacade;
    }

    [HttpGet("[controller]/{id:guid}")]
    [ActionName("")]
    public async Task<IActionResult> Index(Guid id)
    {
        var movie = await _movieFacade.Get(id);
        return View("Index", movie);
    }
}
