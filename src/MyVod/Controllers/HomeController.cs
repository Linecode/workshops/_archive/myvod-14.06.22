﻿using System.Diagnostics;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.Legal.Application;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Sales.B2b.Subscription.Application.Commands;
using MyVod.Domain.SharedKernel;
using MyVod.Facades;
using MyVod.Models;
using MyVod.Services;

namespace MyVod.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IMovieService _movieService;
    private readonly Domain.MoviesCatalog.Application.IMovieService _newMovieService;
    private readonly ILicenseService _licenseService;
    private readonly IMovieFacade _movieFacade;
    private readonly ISender _sender;

    public HomeController(ILogger<HomeController> logger,
        IMovieService movieService,
        Domain.MoviesCatalog.Application.IMovieService newMovieService,
        ILicenseService licenseService,
        IMovieFacade movieFacade, 
        ISender sender)
    {
        _logger = logger;
        _movieService = movieService;
        _newMovieService = newMovieService;
        _licenseService = licenseService;
        _movieFacade = movieFacade;
        _sender = sender;
    }

    public async Task<IActionResult> Index()
    {
        // var movies = await _movieService.GetLive();
        var movies = await _movieFacade.GetLive();
        return View(movies);
    }

    public async Task<IActionResult> Privacy([FromServices] IAggregateRepository repo)
    {
        // var movieId = MovieId.Parse("53341AD3-0185-4ECD-80B8-9F12FEA96370");
        // var licenseId = LicenseId.Parse("F9EE808A-75DE-4F95-AAFA-582C668BD611");
        // // var movie = await _newMovieService.Get(movieId);
        // //
        // // await _newMovieService.ChangeMetadata(movieId, "Poland", DateTime.UtcNow.Subtract(TimeSpan.FromDays(2)));
        // // var dateRange = DateRange.Create(DateTime.UtcNow.Subtract(TimeSpan.FromDays(3)),
        // //     DateTime.UtcNow.Add(TimeSpan.FromDays(3)));
        // // await _licenseService.Save(dateRange, RegionIdentifier.Poland, movieId);
        //
        // // var license = await _licenseService.Get(licenseId);
        //
        // var licenses = await _licenseService.GetPassed();
        var result = await _sender.Send(new CreateOrderCommand(new Guid("37DC0EAF-3A34-49A2-8208-E67539FD078E")));
        
        return result.Match<IActionResult>(
            success: data => Redirect(data.RedirectUrl.ToString()),
            error: _ => StatusCode(500)
        );

        // var order = await repo.Load<MyVod.Domain.Sales.B2b.Subscription.Domain.Order>(
        //     new Guid("de3377c9-d8f2-4b5d-9339-41512779d4b7"));
        
        // return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
