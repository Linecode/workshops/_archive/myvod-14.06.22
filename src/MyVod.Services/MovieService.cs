using Microsoft.EntityFrameworkCore;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Models;

namespace MyVod.Services;

public interface IMovieService
{
    Task Create(Movie movie);
    Task Delete(Movie movie);
    Task Delete(Guid id);
    Task Update(Movie movie);
    Task<Movie> Get(Guid id);
    IQueryable<Movie> Get();
    Task<IEnumerable<Movie>> GetLive();
}

public class MovieService : IMovieService
{
    private readonly MoviesDbContext _context;

    public MovieService(MoviesDbContext context)
    {
        _context = context;
    }

    public async Task Create(Movie movie)
    {
        movie.Id = Guid.NewGuid();

        _context.Movies.Add(movie);
        await _context.SaveChangesAsync();
    }

    public async Task Delete(Movie movie)
    {
        _context.Movies.Remove(movie);
        await _context.SaveChangesAsync();
    }

    public async Task Delete(Guid id)
    {
        _context.Movies.Remove(new Movie { Id = id });
        await _context.SaveChangesAsync();
    }

    public async Task Update(Movie movie)
    {
        _context.Movies.Update(movie);
        await _context.SaveChangesAsync();
    }

    public Task<Movie> Get(Guid id)
    {
        return _context.Movies.Include(x => x.Director)
            .Include(x => x.Genre)
            .FirstOrDefaultAsync(x => x.Id.Equals(id))!;
    }

    public IQueryable<Movie> Get()
    {
        return _context.Movies
            .Include(x => x.Director)
            .Include(x => x.Genre)
            .AsNoTracking()
            .AsQueryable();
    }

    public async Task<IEnumerable<Movie>> GetLive()
    {
        return await _context.Movies.Include(x => x.Director)
            .Include(x => x.Genre)
            .Where(x => x.Status == MovieStatus.Published)
            .Where(x => x.AvailableFrom < DateTime.UtcNow && x.AvailableTo > DateTime.UtcNow)
            .AsNoTracking()
            .ToListAsync();
    }
}