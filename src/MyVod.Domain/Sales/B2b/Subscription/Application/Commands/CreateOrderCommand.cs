using MediatR;
using MyVod.Common.BuildingBlocks.Common;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.CannonicalModel.PublishedLanguage;
using MyVod.Domain.Marketing.Application;
using MyVod.Domain.Sales.B2b.Subscription.Application.Ports;
using MyVod.Domain.Sales.B2b.Subscription.Domain;

namespace MyVod.Domain.Sales.B2b.Subscription.Application.Commands;

public record CreateOrderCommand(Guid SubscriptionTypeId) : IRequest<Result<PaymentIntend>>;

internal class CreateOrderHandler : IRequestHandler<CreateOrderCommand, Result<PaymentIntend>>
{
    private readonly ISubscriptionTypeService _subscriptionTypeService;
    private readonly IAggregateRepository _repository;
    private readonly IPaymentService _paymentService;

    public CreateOrderHandler(ISubscriptionTypeService subscriptionTypeService, IAggregateRepository repository, IPaymentService paymentService)
    {
        _subscriptionTypeService = subscriptionTypeService;
        _repository = repository;
        _paymentService = paymentService;
    }
    
    public async Task<Result<PaymentIntend>> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
    {
        var subscriptionType = await _subscriptionTypeService.Get(request.SubscriptionTypeId);

        if (subscriptionType is null)
            throw new Exception();

        // Here we have to make few trade offs 
        // For current implementation we are missing compensation action if _repository.Store will fail
        // It was omitted because it strongly depend on system
        // It could be done as a saga, but we need to return the: "ReturnUrl" to upper layer
        var order = new Order(subscriptionType);

        var intend = await _paymentService.CreatePaymentIntendForSubscription(subscriptionType.Id.ToString(),
            subscriptionType.Price.Value, subscriptionType.Price.Currency.ToString(), order.Id);
        
        order.AttachIntend(intend);

        await _repository.Store(order, cancellationToken);
        
        return Result<PaymentIntend>.Success(intend);
    }
}