using EnsureThat;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using MyVod.Common.BuildingBlocks.EventSourcing;
using MyVod.Domain.CannonicalModel.PublishedLanguage;

namespace MyVod.Domain.Sales.B2b.Subscription.Domain;

public class Subscription : AggregateBase
{
    public Guid FromOrder { get; private set; }
    public SubscriptionType SubscriptionType { get; private set; }

    public SubscriptionStatus Status { get; private set; } = SubscriptionStatus.Active;
    
    private Subscription(){}
    
    public Subscription(Guid orderId, SubscriptionType subscriptionType)
    {
        Ensure.That(orderId).IsNotDefault();
        Ensure.That(subscriptionType).IsNotNull();
        
        Id = Guid.NewGuid();

        var @event = new SubscriptionEvents.SubscriptionCreated
            { FromOrder = orderId, SubscriptionType = subscriptionType, SubscriptionId = Id };

        Apply(@event);
        AddUncommittedEvent(@event);
    }

    private void Apply(SubscriptionEvents.SubscriptionCreated @event)
    {
        FromOrder = @event.FromOrder;
        SubscriptionType = @event.SubscriptionType;

        Version++;
    }

    public void Deactivate()
    {
        if (Status == SubscriptionStatus.Active)
        {
            var @event = new SubscriptionEvents.SubscriptionDeactivated();
            
            Apply(@event);
            AddUncommittedEvent(@event);
        }
    }

    private void Apply(SubscriptionEvents.SubscriptionDeactivated _)
    {
        Status = SubscriptionStatus.Deactivated;
    }

    public enum SubscriptionStatus
    {
        Active,
        Deactivated
    }
}

public static class SubscriptionEvents
{
    public sealed class SubscriptionCreated
    {
        public Guid FromOrder { get; init; }
        public SubscriptionType SubscriptionType { get; init; }
        public Guid SubscriptionId { get; init; }
    }

    public sealed class SubscriptionDeactivated
    {
    }
}
