using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Domain.Legal.Infrastructure;

public interface ILicenseRepository : IRepository
{
    void Add(License license);
    ValueTask<License?> Get(LicenseId id);
    Task<List<License?>> Get(Specification<License> spec);
}