using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Domain.Specifications;

public class MovieRegionSpecification : Specification<Movie>
{
    private readonly RegionIdentifier _regionIdentifier;

    public MovieRegionSpecification(RegionIdentifier regionIdentifier)
    {
        _regionIdentifier = regionIdentifier;
    }

    public override Expression<Func<Movie, bool>> ToExpression()
        => movie => movie.RegionIdentifier.Equals(_regionIdentifier);
}