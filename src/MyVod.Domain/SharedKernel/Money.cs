using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.SharedKernel;

public class Money : ValueObject<Money>
{
    public int Value { get; private set; }
    public Currencies Currency { get; private set; } = Currencies.Usd;

    [Obsolete("Only for EF Core", true)]
    private Money()
    {
    }
    
    private Money(int value, Currencies currency)
    {
        Value = value;
        Currency = currency;
    }

    public static Money Create(int value, Currencies currency = Currencies.Usd)
    {
        Ensure.That(value).IsGte(0);

        return new Money(value, currency);
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
    
    public enum Currencies
    {
        Usd
    }
}
