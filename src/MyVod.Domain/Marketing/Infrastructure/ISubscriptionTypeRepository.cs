using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Marketing.Domain;

namespace MyVod.Domain.Marketing.Infrastructure;

public interface ISubscriptionTypeRepository : IRepository
{
    void Add(SubscriptionType subscriptionType);
    Task<List<SubscriptionType>> Get();
    Task<SubscriptionType?> Get(Guid id);
}