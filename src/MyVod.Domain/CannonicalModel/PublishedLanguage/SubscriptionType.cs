using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.CannonicalModel.PublishedLanguage;

public class SubscriptionType : ValueObject<SubscriptionType>
{
    public Guid Id { get; private set; }
    public Money Price { get; private set; }
    public TimeSpan TimeSpan { get; private set; }
    
    [Obsolete("Only for EF", true)]
    private SubscriptionType(){}

    private SubscriptionType(Guid id, int price, TimeSpan timeSpan)
    {
        Id = id;
        Price = Money.Create(price);
        TimeSpan = timeSpan;
    }

    public static SubscriptionType Create(Guid id, int price, TimeSpan timeSpan)
    {
        Ensure.That(id).IsNotDefault();
        Ensure.That(price).IsInRange(1, int.MaxValue);
        Ensure.That(timeSpan).IsNotDefault();

        return new(id, price, timeSpan);
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Id;
        yield return Price;
        yield return TimeSpan;
    }
}