using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.Infrastructure;
using MyVod.Domain.MoviesCatalog.ReadModel;
using MyVod.Domain.SharedKernel;

namespace MyVod.Infrastructure.MoviesCatalog;

public class MovieRepository : IMovieRepository
{
    private readonly NewMovieDbContext _context;

    public IUnitOfWork UnitOfWork => _context;

    public MovieRepository(NewMovieDbContext context)
    {
        _context = context;
    }
    
    public async Task<Movie?> Get(MovieId id)
    {
        return await _context.Movies
            .Include(x => x.Director)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<IEnumerable<Movie?>> Get(Specification<Movie> spec)
    {
        return await _context.Movies
            .Where(spec.ToExpression()!)
            .Include(x => x.Director)
            .ToListAsync();
    }

    public async Task<IEnumerable<MovieReadModel?>> GetReadModel(Specification<Movie> spec)
    {
        // This should be mapped to raw Sql query, or we could use Select linq method to not load every column from database
        var movies = await _context.Movies.Where(spec.ToExpression()!)
            .AsNoTracking()
            .ToListAsync();

        return movies.Select(MovieReadModel.From!);
    }

    public void Update(Movie movie)
    {
        _context.Movies.Update(movie);
    }
}