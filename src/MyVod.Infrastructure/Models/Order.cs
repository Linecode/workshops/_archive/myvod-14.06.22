namespace MyVod.Infrastructure.Models;

public class Order
{
    public Guid Id { get; set; }
    
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Address { get; set; }
    public string AddressSecond { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }

    public Guid MovieId { get; set; }
    public int MoviePrice { get; set; }
    public int TaxRate { get; set; }
    public Guid UserId { get; set; }
    
    public OrderStatus Status { get; set; }
}

public enum OrderStatus
{
    Pending,
    Completed,
    Error
}