using Microsoft.EntityFrameworkCore;
using MyVod.Infrastructure.Models;

namespace MyVod.Infrastructure;

public class MoviesDbContext : DbContext
{
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Genre> Genres { get; set; }
    public DbSet<Person> Persons { get; set; }
    public DbSet<Order> Orders { get; set; }

    public MoviesDbContext(DbContextOptions<MoviesDbContext> options) : base(options)
    {
        
    }
}