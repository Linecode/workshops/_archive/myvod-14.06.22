using Marten;
using MyVod.Common.BuildingBlocks.EventSourcing;

namespace MyVod.Infrastructure.EventStore;

public class AggregateRepository : IAggregateRepository
{
    private readonly IDocumentStore _store;

    public AggregateRepository(IDocumentStore store)
    {
        _store = store;
    }

    public async Task Store(AggregateBase aggregate, CancellationToken cancellationToken = default)
    {
        await using (var session = _store.OpenSession())
        {
            // Take non-persisted events, push them to the event stream, indexed by the aggregate ID
            var events = aggregate.GetUncommittedEvents().ToArray();
            session.Events.Append(aggregate.Id, aggregate.Version, events);
            await session.SaveChangesAsync(cancellationToken);
        }
        // Once successfully persisted, clear events from list of uncommitted events
        aggregate.ClearUncommittedEvents();
    }

    public async Task<T> Load<T>(Guid id, int? version = null, CancellationToken cancellationToken = default) where T : AggregateBase
    {
        await using var session = _store.LightweightSession();
        var aggregate = await session.Events.AggregateStreamAsync<T>(id, version ?? 0, token: cancellationToken);
        return aggregate ?? throw new InvalidOperationException($"No aggregate by id {id}.");
    }
}