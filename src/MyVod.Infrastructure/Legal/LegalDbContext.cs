using MediatR;
using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Common.BuildingBlocks.Extensions;
using MyVod.Domain.Legal;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Infrastructure.Legal;

public class LegalDbContext : DbContext, IUnitOfWork
{
    private readonly IMediator _mediator;
    public DbSet<License?> Licenses { get; private set; } = null!;
    
    public LegalDbContext(DbContextOptions<LegalDbContext> options, IMediator mediator) : base(options)
    {
        _mediator = mediator;
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await _mediator.DispatchDomainEventsAsync<LegalDbContext>(this);

        try
        {
            await SaveChangesAsync(cancellationToken);

            return true;
        }
        catch (Exception e)
        {
            throw;
        }
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.ApplyConfigurationsFromAssembly(typeof(LegalDbContext).Assembly);
        modelBuilder.ApplyConfiguration(new LicenseEntityTypeConfiguration());

        base.OnModelCreating(modelBuilder);
    }
}