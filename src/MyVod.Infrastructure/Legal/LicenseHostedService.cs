using System.Diagnostics;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyVod.Domain.BusinessProcess.AcquireLicense;
using MyVod.Domain.CannonicalModel.Events;
using MyVod.Domain.Legal.Application;
using OpenSleigh.Core.Messaging;

namespace MyVod.Infrastructure.Legal;

public class LicenseHostedService : BackgroundService
{
    private readonly IServiceProvider _services;

    public LicenseHostedService(IServiceProvider services)
    {
        _services = services;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(1000, stoppingToken);

            using var scope = _services.CreateScope();

            var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();
            var licenseService = scope.ServiceProvider.GetRequiredService<ILicenseService>();

            var passed = await licenseService.GetPassed();

            foreach (var license in passed)
            {
                var @event = new LicenseExpiredEvent(license!.Id, license.MovieId);
                await mediator.Publish(@event, stoppingToken);
            }

            var messageBus = scope.ServiceProvider.GetRequiredService<IMessageBus>();
            var acquired = await licenseService.GetAcquired();
            
            foreach (var license in acquired)
            {
                var startSagaEvent = new StartAcquireLicenseSaga(Guid.NewGuid(),
                    Guid.NewGuid(),
                    license!.Id,
                    license.MovieId,
                    license.RegionIdentifier);
            
                await messageBus.PublishAsync(startSagaEvent, stoppingToken);
            }
            
            await Task.Delay(31000, stoppingToken);
        }
    }
}
