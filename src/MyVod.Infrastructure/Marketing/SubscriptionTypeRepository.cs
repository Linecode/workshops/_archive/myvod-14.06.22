using LamarCodeGeneration.Util;
using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.Marketing.Domain;
using MyVod.Domain.Marketing.Infrastructure;

namespace MyVod.Infrastructure.Marketing;

public class SubscriptionTypeRepository : ISubscriptionTypeRepository
{
    private readonly SubscriptionTypeDbContext _context;
    public IUnitOfWork UnitOfWork => _context;

    public SubscriptionTypeRepository(SubscriptionTypeDbContext context)
    {
        _context = context;
    }
    
    public void Add(SubscriptionType subscriptionType)
    {
        _context.SubscriptionTypes.Add(subscriptionType);
    }

    public Task<List<SubscriptionType>> Get()
        => _context.SubscriptionTypes.ToListAsync();

    public Task<SubscriptionType?> Get(Guid id)
        => _context.SubscriptionTypes.FirstOrDefaultAsync(x => x.Id == id);
}